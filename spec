#!/bin/bash

BASEDIR=`dirname $0`

if [[ "$1" != "" ]]; then

	URL=`awk -v TGT=$1 '$1==TGT { print $2 }' $BASEDIR/urls.txt`
	if [[ "$URL" != "" ]]; then 
		open $URL
	fi

fi

	
